<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Admin', 'email'  => 'admin@example.com', 'password' => bcrypt(12345), 'role_id' => 1, 'remember_token' => null,],
            ['id' => 2, 'name' => 'Scott', 'email'  => 'scott@example.com', 'password' => bcrypt(12345), 'role_id' => 2, 'remember_token' => null,],
            ['id' => 3, 'name' => 'User01', 'email' => 'user01@example.com', 'password' => bcrypt(12345), 'role_id' => 2, 'remember_token' => null,],
            ['id' => 4, 'name' => 'User02', 'email' => 'user02@example.com', 'password' => bcrypt(12345), 'role_id' => 2, 'remember_token' => null,],
            ['id' => 5, 'name' => 'User03', 'email' => 'user03@example.com', 'password' => bcrypt(12345), 'role_id' => 2, 'remember_token' => null,],
        ];

        foreach ($items as $item) {
            \App\Models\User::create($item);
        }
    }
}
