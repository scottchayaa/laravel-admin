<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomizedTables extends Migration
{
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('card_no', 20)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('cellphone', 45)->nullable();
            $table->date('birthday')->nullable();
            $table->string('address', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 255)->nullable();
            $table->rememberToken();
            $table->nullableTimestamps();

            $table->index(["email"]);
        });

        Schema::create('members_line', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('members_id');
            $table->string('uid', 50)->unique();
            $table->string('displayname', 255);
            $table->string('pictureUrl', 255);
            $table->tinyInteger('friendFlag')->default(0)->comment('LINE 好友狀態 : 1 加入, 0 封鎖(未加入)');
            $table->string('email', 255)->nullable();
            $table->string('phone', 100)->nullable()->comment('(可要求LINE官方開通取得權限)');
            $table->nullableTimestamps();

            $table->primary('members_id');

            $table->foreign('members_id')
                ->references('id')
                ->on('members')
                ->onDelete('NO ACTION');
        });
    }

    public function down()
    {
        Schema::dropIfExists('members_line');
        Schema::dropIfExists('members');
    }
}
