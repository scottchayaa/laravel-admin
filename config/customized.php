<?php

return [
  'line' => [
    'login' => [
        'channel' => env('LINE_LOGIN_CHANNEL', ''),
        'secret' => env('LINE_LOGIN_SECRET', ''),
    ],
    'messaging' => [
        'channel' => env('LINE_MESSAGING_CHANNEL', ''),
        'secret' => env('LINE_MESSAGING_SECRET', ''),
        'long_token' => env('LINE_MESSAGING_LONG_TOKEN', ''),
        'line_me' => env('LINE_MESSAGEING_LINE_ME', ''),
    ]
  ],

  'sms' => [
      'otp_ttl' => 60,
  ],
];
