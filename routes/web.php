<?php

/**
 * Web Routes
 *
 * namespace : Web\
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/member/line/login', 'Auth\LoginController@lineLogin'); // Line Login redirect 回頁的統一路口

Route::group(['middleware' => ['auth.line', 'checkMemberBind']], function () {
    Route::get('/member/agreement', 'MemberController@showAgreement');
    Route::get('/member/inputdata', 'MemberController@showInputData');
    Route::post('/member/checkphone', 'MemberController@showCheckPhone');
    Route::post('/member/sendsmsotp', 'MemberController@sendSmsOtp');
    Route::post('/member/verifysmsotp', 'MemberController@verifySmsOtp');
    Route::get('/member/success', 'MemberController@showSuccess');
});
