<?php

/**
 * Admin Routes
 *
 * prefix : /admin
 * namespace : Web\
 */

Route::get('/', function () {
    return redirect('/admin/login');
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');

// Register Routes...
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('auth.change_password');
Route::post('/register', 'Auth\RegisterController@register')->name('auth.change_password');

// Password Reset Routes
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

// Change Password Routes...
Route::get('/change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('/change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });

    Route::get('/members', 'Member\Index@index');

    Route::get('/users', 'UserController@index');
    Route::get('/users/create', 'UserController@create');
    Route::post('/users', 'UserController@store');
    Route::get('/users/{id}/edit', 'UserController@edit');
    Route::put('/users/{id}', 'UserController@update');
    Route::delete('/users/{id}', 'UserController@delete');

    Route::get('/richmenus', 'Richmenu\Index');
    Route::get('/richmenus/create', 'Richmenu\Create');
});
