@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Change Password</h1>
@stop

@section('content')

<form method="POST" action="/admin/change_password">
<div class="box box-default">
    <div class="box-heading">

    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="control-label">目前密碼</label>
                <input name="current_password" type="password" class="form-control">
                <p class="help-block"></p>
                @if($errors->has('current_password'))
                    <p class="help-block">
                        {{ $errors->first('current_password') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="control-label">新密碼</label>
                <input name="new_password" type="password" class="form-control">
                <p class="help-block"></p>
                @if($errors->has('new_password'))
                    <p class="help-block">
                        {{ $errors->first('new_password') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="control-label">新密碼</label>
                <input name="new_password_confirmation" type="password" class="form-control">
                <p class="help-block"></p>
                @if($errors->has('new_password_confirmation'))
                    <p class="help-block">
                        {{ $errors->first('new_password_confirmation') }}
                    </p>
                @endif
            </div>
        </div>
    </div>


    <div class="box-footer">
        <button type="submit" class="btn btn-danger" >確認</button>
    </div>
</div>

</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script></script>
@stop
