@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>帳號管理</h1>
@stop

@section('content')

{{-- Search Form --}}
<div class="card card-default">
    <form id="searchForm" method="GET">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <label>Name</label>
                <input type="text" name="name" class="form-control" value="{{ request('name') }}" autofocus>
            </div>
            <div class="col-md-3">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="{{ request('email') }}">
            </div>
        </div>
    </div>
    </form>
    <div class="card-footer">
        <button id="search" class="btn btn-sm btn-info"><i class="fa fa-search"></i> 搜尋</button>
        <button id="reset" class="btn btn-sm btn-warning"><i class="fa fa-eraser"></i> 清除</button>
    </div>
</div>

{{-- DataTable --}}
<div class="card card-default">
    <div class="card-header">
        <a class="btn btn-sm btn-success" href="users/create">
            <i class="fa fa-plus"></i> 新增
        </a>
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 20px">#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>權限</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}
                            @if (auth()->user()->id === $user->id)
                                <span style="color:red;">(it's you)</span>
                            @endif
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role->title }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="users/{{ $user->id }}/edit">
                                編輯
                            </a>

                            <form method="POST" action="users/{{ $user->id }}" style="display: inline-block;"
                                onsubmit="return confirm('確認刪除？')">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit">
                                    刪除
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        {{ $users->appends(request()->except('page'))->links() }}
    </div>
</div>

@stop

@section('css')

@stop

@section('js')
<script>
    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });

        form.find(':input').keypress(function (event) {
            if (event.keyCode == 13) {
                form.submit();
            }
        });
    }

    init();
</script>
@stop
