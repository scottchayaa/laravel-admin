@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>帳號管理 > 新增</h1>
@stop

@section('content')

<div class="card card-success">
    {{-- <div class="card-header">
        <h3 class="card-title">新增</h3>
    </div> --}}
    <form method="post" action="./" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label>New Password</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="form-group">
                <label>權限</label>
                <select name="role_id" class="form-control" >
                    @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>頭像</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input">
                        <label class="custom-file-label">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Save</button>
            <a class="btn btn-default" href="{{ Session::get('LIST_URL')}}">Cancel</a>
        </div>
    </form>
</div>

@stop

@section('css')

@stop

@section('js')

@stop
