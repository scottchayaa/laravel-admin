@extends('adminlte::page')

@section('css')

@stop

@section('title', 'Dashboard')

@section('content_header')
    <h1>Richmenu 管理 > 新增</h1>
@stop

@section('content')

<form method="post" action="./" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">Basic Data</h3>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label>chatBarText</label>
                <input type="text" class="form-control" name="chatBarText">
            </div>
            <div class="form-group">
                <label>selected</label>
                <select name="selected" class="form-control" >
                    <option value="1" selected>true</option>
                    <option value="0">false</option>
                </select>
            </div>
            <div class="form-group">
                <label>Richmenu Image</label>
                <input type="file" id="fileRichmenuImage">
            </div>
        </div>
    </div>

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">Areas</h3>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Bounds</label><br>
                        <textarea class="form-control" rows="5" name="bounds"></textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Actions</label><br>
                        <textarea class="form-control" rows="5" name="actions"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-danger">Save</button>
    <a class="btn btn-default" href="{{ Session::get('LIST_URL')}}">Cancel</a>
</form>

@stop

@section('js')
<script>
    $('#fileRichmenuImage').fileinput({
        showUpload: false,
        maxFileSize: 1000,
        allowedFileExtensions: ['jpg', 'jpeg']
    });
</script>
@stop
