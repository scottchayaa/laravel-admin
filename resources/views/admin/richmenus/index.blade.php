@extends('adminlte::page')

@section('title', 'Dashboard')

@section('css')
@stop

@section('content_header')
    <h1>Richmenu 管理</h1>
@stop

@section('content')

{{-- Search Form --}}
<div class="card card-default">
    <form id="searchForm" method="GET">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <label>Name</label>
                <input type="text" name="name" class="form-control" value="{{ request('name') }}" autofocus>
            </div>
        </div>
    </div>
    </form>
    <div class="card-footer">
        <button id="search" class="btn btn-sm btn-info"><i class="fa fa-search"></i> 搜尋</button>
        <button id="reset" class="btn btn-sm btn-warning"><i class="fa fa-eraser"></i> 清除</button>
    </div>
</div>

{{-- DataTable --}}
<div class="card card-default">
    <div class="card-header">
        <a class="btn btn-sm btn-success" href="richmenus/create">
            <i class="fa fa-plus"></i> 新增
        </a>
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th >Name</th>
                    <th style="width: 20%">發布時間</th>
                    <th style="width: 20%">建立時間</th>
                    <th style="width: 20%"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($richmenus as $richmenu)
                    <tr>
                        <td>{{ $richmenu->id }}</td>
                        <td>{{ $richmenu->name }}</td>
                        <td>{{ $richmenu->published_at }}</td>
                        <td>{{ $richmenu->created_at }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="richmenus/{{ $richmenu->id ?? ''}}/edit">
                                編輯
                            </a>

                            <form method="POST" action="richmenus/{{ $richmenu->id ?? '' }}" style="display: inline-block;"
                                onsubmit="return confirm('確認刪除？')">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit">
                                    刪除
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        {{ $richmenus->appends(request()->except('page'))->links() }}
    </div>
</div>

@stop

@section('css')

@stop

@section('js')
<script>
    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });

        form.find(':input').keypress(function (event) {
            if (event.keyCode == 13) {
                form.submit();
            }
        });
    }

    init();
</script>
@stop
