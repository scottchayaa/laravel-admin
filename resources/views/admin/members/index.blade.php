@extends('adminlte::page')

@section('title', 'Dashboard')

@section('css')
<style>
    .line_block_img {
        width: 80px;
        text-align: center;
    }

    .line_block_img img {
        width: 60px;
        height: 60px;
        border-radius: 50%;
    }

    .line_block_uid {
        color: #999;
    }
</style>
@stop

@section('content_header')
    <h1>會員管理</h1>
@stop

@section('content')

{{-- Search Form --}}
<div class="card card-default">
    <form id="searchForm" method="GET">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <label>Name</label>
                <input type="text" name="name" class="form-control" value="{{ request('name') }}" autofocus>
            </div>
            <div class="col-md-3">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="{{ request('email') }}">
            </div>
        </div>
    </div>
    </form>
    <div class="card-footer">
        <button id="search" class="btn btn-sm btn-info"><i class="fa fa-search"></i> 搜尋</button>
        <button id="reset" class="btn btn-sm btn-warning"><i class="fa fa-eraser"></i> 清除</button>
    </div>
</div>

{{-- DataTable --}}
<div class="card card-default">
    <div class="card-header">
        <a class="btn btn-sm btn-success" href="users/create">
            <i class="fa fa-plus"></i> 新增
        </a>
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th style="width: 15%">Profile Info</th>
                    <th>LINE</th>
                    <th style="width: 20%">建立時間</th>
                    <th style="width: 15%"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($members as $member)
                    <tr>
                        <td>{{ $member->id }}</td>
                        <td>
                            {!! $member->profile_info !!}
                        </td>
                        <td>
                            <div class="row">
                                <div class="line_block_img">
                                    <img src="{{ $member->line->pictureUrl ?? '' }}">
                                </div>
                                <div class="line_block_info">
                                    {{ $member->line->displayname ?? '' }}
                                    <br>
                                    <span class="line_block_uid">{{ $member->line->uid ?? '' }}</span>
                                </div>
                            </div>
                        </td>
                        <td>{{ $member->created_at }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="members/{{ $member->id ?? ''}}/edit">
                                編輯
                            </a>

                            <form method="POST" action="members/{{ $member->id ?? '' }}" style="display: inline-block;"
                                onsubmit="return confirm('確認刪除？')">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit">
                                    刪除
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        {{ $members->appends(request()->except('page'))->links() }}
    </div>
</div>

@stop

@section('css')

@stop

@section('js')
<script>
    function init() {
        const form = $('#searchForm');
        common.initForm(form);

        $('#search').click(function () {
            form.submit();
        });
        $('#reset').click(function () {
            common.resetForm(form);
        });

        form.find(':input').keypress(function (event) {
            if (event.keyCode == 13) {
                form.submit();
            }
        });
    }

    init();
</script>
@stop
