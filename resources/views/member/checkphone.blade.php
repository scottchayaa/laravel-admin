<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input</title>
</head>
<body>
<form method="post" action="/member/verifysmsotp" onsubmit="return next(this)">
    {{ csrf_field() }}
    簡訊驗證碼 <input type="text" name="otp_number" required>
    <input type="hidden" name="cellphone" value="{{ $cellphone }}">
    <br>
    <button type="button" id="btn_resend" onclick="resend('{{ $cellphone }}')">重發驗證碼</button><br>
    <span id="otp_ttl">{{ $otp_ttl }}</span><br>
    <br>

    <button type="submit">下一步</button>
</form>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.1/axios.js"></script>
<script>
    var otp_ttl = {{ $otp_ttl }};
    var timerCountdown;

    function countDown()
    {
        $('#otp_ttl').html(otp_ttl--);
        timerCountdown = setTimeout('countDown()', 1000);

        if (otp_ttl <= 0) {
            clearTimeout(timerCountdown);
            $('#btn_resend').show();
            $('#otp_ttl').hide();
        } else {
            $('#btn_resend').hide();
            $('#otp_ttl').show();
        }
    }

    function resend(cellphone) {
        var formData = new FormData;
        formData.append('cellphone', cellphone);
        axios.post('/member/sendsmsotp', formData, {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        })
        .then(res => {
            console.log(res.data);
            location.reload();
        })
        .catch(err => {
            console.log(err.response.data);
            alert(err.response.data.msg);
        });
    }

    function next(form) {
        var formData = new FormData(form);
        axios.post('/member/verifysmsotp', formData, {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        })
        .then(res => {
            console.log(res.data);
            location.href = '/member/success';
        })
        .catch(err => {
            console.log(err.response.data);
            alert(err.response.data.msg);
        });

        return false;
    }

    countDown();
</script>
</body>
</html>
