<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input</title>
</head>
<body>
    <form method="POST" action="/member/checkphone" onsubmit="return send()">
    {{ csrf_field() }}
    * 姓名 <input type="text" name="name" required>
    <br>
    * 電話 <input type="text" id="cellphone" name="cellphone" required>
    <br>
    生日 <input type="text" name="birthday" >
    <br>
    <button type="submit">下一步</button>
    </form>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.1/axios.js"></script>
<script>
    function send() {
        var post = new FormData;
        post.append('cellphone', document.querySelector('#cellphone').value);
        axios.post('/member/sendsmsotp', post, {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }).then(res => {
            document.querySelector('form').submit();
        });

        return false;
    }
</script>
</html>
