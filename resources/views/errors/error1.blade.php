<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error</title>
</head>
<body>
    <h4>{{ $e->getMessage() }}</h4>
    <a href="{{ $backUrl ?? config('customized.line.messaging.line_me') }}">{{ $backText ?? '返回官方帳號' }}</a>
</body>
</html>
