<?php

return [

    'full_name'                   => '姓名',
    'email'                       => '信箱',
    'password'                    => '密碼',
    'retype_password'             => '重新輸入密碼',
    'remember_me'                 => '記住我',
    'register'                    => '註冊',
    'register_a_new_membership'   => '註冊新用戶',
    'i_forgot_my_password'        => '忘記密碼',
    'i_already_have_a_membership' => '已有帳戶',
    'sign_in'                     => '登入',
    'log_out'                     => '退出',
    'toggle_navigation'           => '切换导航',
    'login_message'               => '請先登入',
    'register_message'            => '註冊新用戶',
    'password_reset_message'      => '重製密碼',
    'reset_password'              => '重製密碼',
    'send_password_reset_link'    => '發送密碼重置連結',
];
