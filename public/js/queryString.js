const queryString = {
  q: new URLSearchParams(window.location.search),
  toString() { return window.location.search; },
  get(key) { return this.q.get(key)},
};