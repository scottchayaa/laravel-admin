<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['title'];
    protected $hidden = [];

    public static function boot()
    {
        parent::boot();

        // Role::observe(new \App\Observers\UserActionsObserver);
    }
}
