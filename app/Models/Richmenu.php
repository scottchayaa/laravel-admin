<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Richmenu extends Model
{
    protected $table = 'richmenus';
    protected $primaryKey = 'id';

    protected $fillable = [
        'richMenuId',
        'name',
        'chatBarText',
        'areas',
        'selected',
        'published_at',
    ];
    protected $hidden = [];
}
