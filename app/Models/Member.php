<?php

namespace App\Models;

use App\Models\MemberLine;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Member extends Authenticatable
{
    use Notifiable;

    protected $table = 'members';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'card_no',
        'cellphone',
        'birthday',
        'address',
        'email',
        'password'
    ];
    protected $hidden = ['password'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = Hash::needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function line()
    {
        return $this->hasOne(MemberLine::class, 'members_id', 'id');
    }

    public function getProfileInfoAttribute()
    {
        $data = $this->only(['name', 'email', 'cellphone', 'birthday', 'address']);

        $data = array_filter($data, function ($value) {
            return !is_null($value) && $value !== '';
        });

        return implode("<br>", $data);
    }
}
