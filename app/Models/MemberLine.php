<?php
namespace App\Models;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MemberLine extends Model
{
    use Notifiable;

    protected $table = 'members_line';
    protected $primaryKey = 'members_id';

    protected $fillable = [
        'members_id',
        'displayName',
        'uid',
        'pictureUrl',
        'friendFlag',
        'email',
        'phone',
    ];

    protected $hidden = [];

    public function member()
    {
        return $this->belongsTo(Member::class, 'members_id', 'id');
    }
}
