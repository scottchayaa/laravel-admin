<?php

namespace App\Http\Controllers\Admin\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;

class Index extends Controller
{
    public function index(Request $request)
    {
        $query = Member::orderBy('id', 'desc');

        if (request('name')) {
            $query->where('name', 'like', '%'.request('name').'%');
        }
        if (request('email')) {
            $query->where('email', 'like', '%'.request('email').'%');
        }

        $request->session()->put('LIST_URL', url()->full());

        return view('admin.members.index', [
            'members' => $query->paginate(config('adminlte.pagesize'))
        ]);
    }
}
