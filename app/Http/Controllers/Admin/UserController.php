<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $query = User::with('role')->orderBy('id', 'desc');

        if (request('name')) {
            $query->where('name', 'like', '%'.request('name').'%');
        }
        if (request('email')) {
            $query->where('email', 'like', '%'.request('email').'%');
        }

        $request->session()->put('LIST_URL', url()->full());

        return view('admin.users.index', [
            'users' => $query->paginate(config('adminlte.pagesize'))
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.users.create', [
            'roles' => Role::all()
        ]);
    }

    public function store(Request $request)
    {
        User::create($request->all());
        return redirect($request->session()->get('LIST_URL'));
    }

    public function edit(Request $request, int $id)
    {
        return view('admin.users.edit', [
            'user' => User::find($id),
            'roles' => Role::all()
        ]);
    }

    public function update(Request $request, int $id)
    {
        User::find($id)->update($request->all());
        return redirect($request->session()->get('LIST_URL'));
    }

    public function delete(Request $request, int $id)
    {
        User::findOrFail($id)->delete();
        return redirect($request->session()->get('LIST_URL'));
    }
}
