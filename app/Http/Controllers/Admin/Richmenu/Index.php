<?php

namespace App\Http\Controllers\Admin\Richmenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Richmenu;

class Index extends Controller
{
    public function __invoke(Request $request)
    {
        $query = Richmenu::orderBy('id', 'desc');

        if (request('name')) {
            $query->where('name', 'like', '%'.request('name').'%');
        }

        $request->session()->put('LIST_URL', url()->full());

        return view('admin.richmenus.index', [
            'richmenus' => $query->paginate(config('adminlte.pagesize'))
        ]);
    }
}
