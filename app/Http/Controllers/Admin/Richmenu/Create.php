<?php

namespace App\Http\Controllers\Admin\Richmenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Create extends Controller
{
    public function __invoke(Request $request)
    {
        return view('admin.richmenus.create');
    }
}
