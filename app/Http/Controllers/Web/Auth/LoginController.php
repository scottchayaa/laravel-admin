<?php

namespace App\Http\Controllers\Web\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Line\Login\LineLoginHandlerService;

class LoginController extends Controller
{
    public function __construct(LineLoginHandlerService $lineLoginHandlerService)
    {
        $this->lineLoginHandlerService = $lineLoginHandlerService;
    }

    /**
     * Line Login redirect 回頁的統一路口
     * https://developers.line.biz/en/docs/line-login/web/integrate-line-login/
     *
     * @param Request $request
     * @return void
     */
    public function lineLogin(Request $request)
    {
        $this->lineLoginHandlerService->login($request->code, $request->state);
    }
}
