<?php

namespace App\Http\Controllers\Web;

use App\Models\Member;
use Illuminate\Support\Str;
use App\Services\SmsService;
use Illuminate\Http\Request;
use App\Services\ErrorService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    public function __construct()
    {
    }

    public function showAgreement(Request $request)
    {
        return view('member.agreement');
    }

    public function showInputData(Request $request)
    {
        return view('member.inputdata');
    }

    public function showCheckPhone(Request $request)
    {
        Session::put('userdata', $request->all());

        $otp_ttl = (new SmsService)->getOtpTtl($request->cellphone);

        return view('member.checkphone', [
            'otp_ttl' => $otp_ttl,
            'cellphone' => $request->cellphone
        ]);
    }

    public function sendSmsOtp(Request $request)
    {
        (new SmsService)->createOtp($request->cellphone);
    }

    public function verifySmsOtp(Request $request)
    {
        try {
            (new SmsService)->verifyOtp($request->cellphone, $request->otp_number);

            // 資料填入
            $userdata = Session::pull('userdata');
            $member = Auth::guard('web')->user();
            $member->name = $userdata['name'];
            $member->cellphone = $userdata['cellphone'];
            $member->birthday = $userdata['birthday'];
            $member->card_no = Str::random(10);
            $member->save();
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
            ], 400);
        }

        return response()->json([
            'msg' => 'success'
        ], 200);
    }

    public function showSuccess(Request $request)
    {
        return view('member.success');
    }
}
