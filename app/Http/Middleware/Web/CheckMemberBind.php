<?php

namespace App\Http\Middleware\Web;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckMemberBind
{
    public function __construct()
    {
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $route = $request->path();

        if ($route === 'member/success') {
            if (empty(Auth::guard('web')->user()->card_no)) {
                return redirect('/member/agreement');
            }
        } else {
            if (Auth::guard('web')->user()->card_no) {
                return redirect('/member/success');
            }
        }

        return $next($request);
    }
}
