<?php

namespace App\Http\Middleware\Web;

use Closure;
use App\Services\Line\Login\LineLoginService;
use App\Services\Line\Login\Callbacks\CreateMemberCallback;

class AuthLine
{
    public function __construct(LineLoginService $lineLoginService)
    {
        $this->lineLoginService = $lineLoginService;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $this->lineLoginService->setScope(['email', 'phone', 'profile', 'openid']);
        $this->lineLoginService->setCallback(CreateMemberCallback::class);
        $this->lineLoginService->enablePrompt(false);
        $this->lineLoginService->setRedirectPageUrl(url($request->path()));
        $this->lineLoginService->redirectIfNotLogin();

        return $next($request);
    }
}
