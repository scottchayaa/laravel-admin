<?php

namespace App\Services\Line;

use GuzzleHttp\Client;

class ReplyService
{
    protected $events = null;
    protected $replyMessages = [];
    protected $apiUrl_reply = 'https://api.line.me/v2/bot/message/reply';

    public function setEvents(array $events)
    {
        $this->events = $events;
        return $this;
    }

    public function appendReplyMessages(string $replyToken, array $messageObj)
    {
        $this->replyMessages[] = [
            'replyToken' => $replyToken,
            'messages' => $messageObj,
        ];
    }

    public function parserKeywordReply(string $source, string $target)
    {
        foreach ($this->events as $event) {
            if ($event['type'] == 'message') {
                if ($event['message']['type'] == 'text') {
                    $text = $event['message']['text'];
                    if ($text == $source) {
                        $this->appendReplyMessages($event['replyToken'], [
                            [
                            'type' => 'text',
                            'text' => $target,
                            ]
                        ]);
                    }
                }
            }
        }

        return $this;
    }

    public function dumpReplyMessages()
    {
        \Log::info(json_encode($this->replyMessages, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
        return $this;
    }

    public function reply()
    {
        $results = [];

        foreach ($this->replyMessages as $replyMessage) {
            $client = new Client;

            $response = $client->post($this->apiUrl_reply, [
                'headers' => [
                    'Authorization' => 'Bearer ' . config('customized.line.messaging.long_token')
                ],
                'json' => $replyMessage
            ]);

            array_push($results, json_decode($response->getBody()->getContents(), true));
        }

        return $results;
    }
}
