<?php

namespace App\Services\Line\Login;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Services\Line\Login\Callbacks\LineLoginCallback;

class LineLoginService
{
    public const DEFAULT_ENDPOINT = 'https://access.line.me';
    public const LOGIN_API = '/oauth2/v2.1/authorize';
    public const LOGIN_CALLBACK_URI = '/member/line/login';
    public const SCOPE_PROFILE = 'profile';
    public const SCOPE_OEPNID = 'openid';
    public const SCOPE_EMAIL = 'email';
    public const SCOPE_PHONE = 'phone';
    public const BOT_PROMPT_NORMAL = 'normal';
    public const BOT_PROMPT_AGGRESSIVE = 'aggressive';
    public const PROMPT_CONSENT = 'consent';

    protected $responseType = 'code';
    protected $clientId;
    protected $redirectUrl;
    protected $state;
    protected $scope = 'profile openid';
    protected $botPrompt = 'normal';
    protected $promptEnable = false;
    // protected $prompt;
    // protected $nonce;
    // protected $maxAge;
    // protected $uiLocales;

    private $params = [];

    public function __construct()
    {
        $this->clientId = config('customized.line.login.channel');
        $this->redirectUrl = url(static::LOGIN_CALLBACK_URI);
    }

    /**
     * 是否每次都開啟使用者同意視窗
     *
     * @param boolean $boolean
     * @return void
     */
    public function enablePrompt(bool $boolean): void
    {
        $this->promptEnable = $boolean;
    }

    /**
     * 設定 bot_prompt
     *
     * @param string $botPrompt
     * @return void
     */
    public function setBotPrompt(string $botPrompt): void
    {
        if (!in_array(
            $botPrompt,
            [
                static::BOT_PROMPT_NORMAL,
                static::BOT_PROMPT_AGGRESSIVE,
            ]
        )
        ) {
            throw new \Exception('The bot_prompt value is not available');
        }

        $this->botPrompt = $botPrompt;
    }

    /**
     * 設定 scope， 參照LINE API文件
     *
     * @param array $scopes
     * @return void
     */
    public function setScope(array $scopes): void
    {
        foreach ($scopes as $value) {
            if (!in_array(
                $value,
                [
                    static::SCOPE_PROFILE,
                    static::SCOPE_OEPNID,
                    static::SCOPE_EMAIL,
                    static::SCOPE_PHONE
                ]
            )
            ) {
                throw new \Exception('The scope value is not available');
            }
        }

        $this->scope = implode(' ', $scopes);
    }

    /**
     * 設定額外傳遞參數
     * 預設會將 redirectUrl 設定上去
     *
     * @param array $params
     * @return void
     */
    public function setParams(array $params): void
    {
        $this->params['data'] = $params;
        $this->params['redirectUrl'] = $this->redirectUrl;
    }

    /**
     * 設定 callback class
     * login 完後指定處理 callback handle
     *
     * @param string $callback
     * @return void
     */
    public function setCallback(string $callback): void
    {
        if (!is_subclass_of($callback, LineLoginCallback::class)) {
            throw new \Exception('The callback is not correct.');
        }
        $this->params['callback'] = $callback;
    }

    /**
     * 設定 callback handle 完後導頁網址
     *
     * @param string $url
     * @return void
     */
    public function setRedirectPageUrl(string $url): void
    {
        if (empty($url)) {
            throw new \Exception('The redirect page url is not correct.');
        }
        $this->params['redirectPageUrl'] = $url;
    }

    /**
     * 產生 LineLogin網址
     *
     * @return string
     */
    public function createLoginUrl(): string
    {
        $this->setParams($this->params['data'] ?? []);

        // 強制輸入
        // $this->setCallback($this->callback);
        $this->setRedirectPageUrl($this->params['redirectPageUrl']);
        $lineLoginUrl = static::DEFAULT_ENDPOINT . static::LOGIN_API;
        $lineLoginParams = [
            'response_type' => $this->responseType,
            'client_id' => $this->clientId,
            'scope' => $this->scope,
            'bot_prompt' => $this->botPrompt,
            'redirect_uri' => $this->redirectUrl,
            'state' => encrypt(json_encode($this->params))
        ];

        if ($this->promptEnable) {
            $lineLoginParams['prompt'] = static::PROMPT_CONSENT;
        }

        $lineLoginUrl .= '?' .http_build_query($lineLoginParams, null, '&', PHP_QUERY_RFC3986);
        return $lineLoginUrl;
    }

    /**
     * 產生 LineLogin網址，並檢查SESSION是否有登入
     *
     */
    public function redirectIfNotLogin()
    {
        if (!Auth::guard('web')->check()) {
            header('Location: ' . $this->createLoginUrl());
            exit;
        }
    }

    /**
     * 從 session 取出 line profile data : array
     *
     * @return void
     */
    public function getProfile()
    {
        return Session::get(\App\Services\Line\Login\LineLoginHandlerService::LINE_PROFILE_SESSION_KEY);
    }
}
