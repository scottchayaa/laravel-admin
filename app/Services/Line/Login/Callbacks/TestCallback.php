<?php

namespace App\Services\Line\Login\Callbacks;

class TestCallback extends LineLoginCallback
{
    public function handle(array $profile, array $params)
    {
        dd(['this is test callback', $profile, $params]);
    }
}
