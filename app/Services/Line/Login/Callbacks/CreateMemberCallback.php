<?php

namespace App\Services\Line\Login\Callbacks;

use App\Models\Member;
use App\Models\MemberLine;
use Illuminate\Support\Facades\Auth;
use App\Services\Line\Login\Callbacks\LineLoginCallback;

class CreateMemberCallback extends LineLoginCallback
{
    public function handle(array $profile, array $params)
    {
        $memberLine = MemberLine::where('uid', $profile['userId'])->first();

        if (!$memberLine) {
            $member = Member::create([]);
            $memberLine = MemberLine::create([
                'members_id' => $member->id,
                'uid' => $profile['userId'],
                'displayName' => $profile['displayName'],
                'pictureUrl' => $profile['pictureUrl'],
            ]);
        } else {
            $member = Member::find($memberLine->members_id);
        }

        // Member Login
        Auth::guard('web')->login($member);
    }
}
