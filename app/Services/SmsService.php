<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class SmsService
{
    protected $apiUrl_push = 'https://api.line.me/v2/bot/message/push';
    protected $prefixKey;


    public function getOtpTtl($cellphone)
    {
        return Redis::ttl(Auth::guard('web')->id() . ':' . $cellphone);
    }

    public function createOtp($cellphone)
    {
        $ttl = Redis::ttl(Auth::guard('web')->id() . ':' . $cellphone);
        if ($ttl === -2) {
            $ttl = config('customized.sms.otp_ttl');
            Redis::setex(Auth::guard('web')->id() . ':' . $cellphone, $ttl, rand(1000, 9999));
        }
        return $ttl;
    }

    public function verifyOtp($cellphone, $inputNumber)
    {
        $otp_number = Redis::get(Auth::guard('web')->id() . ':' . $cellphone);

        if (empty($otp_number)) {
            throw new \Exception("OTP number expired or not exist", 1);
            return;
        }

        if ($otp_number != $inputNumber) {
            throw new \Exception("OTP number is not correct", 2);
            return;
        }

        Redis::del(Auth::guard('web')->id() . ':' . $cellphone);
    }
}
