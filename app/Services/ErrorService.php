<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class ErrorService
{
    public function error1($e, $backText = null, $backUrl = null)
    {
        return view('errors.style1', compact('e', 'backText', 'backUrl'));
    }
}
